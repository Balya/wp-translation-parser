<?php
error_reporting(E_ERROR);
ini_set('memory_limit', '512M');

echo date('H:m:s').": Устанавливаю соединение с БД \n";
$db_link = mysql_connect('localhost', 'root', '') or die("Не удалось соединиться: " . mysql_error() . "\n");
echo "Соединение c БД успешно установлено \n";
mysql_select_db('wp_translate') or die("Не удалось выбрать базу данных \n");
// SELECT `parser`.`translation`, count(`parser`.`translation`) as cnt FROM `parser` GROUP BY `parser`.`translation` ORDER BY cnt desc limit 0,500


$file = fopen("base.po", "w");
	// 'r'	Открывает файл только для чтения; помещает указатель в начало файла.
	// 'r+'	Открывает файл для чтения и записи; помещает указатель в начало файла.
	// 'w'	Открывает файл только для записи; помещает указатель в начало файла и обрезает файл до нулевой длины. Если файл не существует - пробует его создать.
	// 'w+'	Открывает файл для чтения и записи; помещает указатель в начало файла и обрезает файл до нулевой длины. Если файл не существует - пытается его создать.
	// 'a'	Открывает файл только для записи; помещает указатель в конец файла. Если файл не существует - пытается его создать.
	// 'a+'	Открывает файл для чтения и записи; помещает указатель в конец файла. Если файл не существует - пытается его создать.
	// 'x'	Создаёт и открывает только для записи; помещает указатель в начало файла. Если файл уже существует, вызов fopen() закончится неудачей, вернёт FALSE и выдаст ошибку уровня E_WARNING. Если файл не существует, попытается его создать. Это эквивалентно указанию флагов O_EXCL|O_CREAT для внутреннего системного вызова open(2).
	// 'x+'	Создаёт и открывает для чтения и записи; иначе имеет то же поведение что и'x'.
	// 'c'	Открывает файл только для записи. Если файл не существует, то он создается. Если же файл существует, то он не обрезается (в отличии от 'w'), и вызов к этой функции не вызывает ошибку (также как и в случае с 'x'). Указатель на файл будет установлен на начало файла. Это может быть полезно при желании заблокировать файл (смотри flock()) перед изменением, так как использование 'w' может обрезать файл еще до того как была получена блокировка (если вы желаете обрезать файл, можно использовать функцию ftruncate() после запроса на блокировку).
	// 'c+'	Открывает файл для чтения и записи; иначе имеет то же поведение, что и 'c'.

$header  = 'msgid ""'."\n";
$header .= 'msgstr ""'."\n";
$header .= '"Project-Id-Version: TransBase v1.0 \n"'."\n";
$header .= '"POT-Creation-Date: 2016-02-25 04:30+0200\n"'."\n";
$header .= '"PO-Revision-Date: 2016-02-25 17:34+0200\n"'."\n";
$header .= '"Last-Translator: Александр Баля <mail@webkit.pro>\n"'."\n";
$header .= '"Language-Team: WebKit.pro \n"'."\n";
$header .= '"Language: ru_RU\n"'."\n";
$header .= '"MIME-Version: 1.0\n"'."\n";
$header .= '"Content-Type: text/plain; charset=UTF-8\n"'."\n";
$header .= '"Content-Transfer-Encoding: 8bit\n"'."\n";
fwrite($file, $header);
fclose ($file);





$trans_query = "SELECT `row`, `translation`, count(`translation`) as `counter` FROM `parser` GROUP BY `translation` ORDER BY `counter` desc limit 0,20000";
$trans_result = mysql_query($trans_query) or die('Запрос на получение переводов не удался: ' . mysql_error());
while ($trans_line = mysql_fetch_array($trans_result, MYSQL_ASSOC)) {
	$row = $trans_line[row];
	$translation = $trans_line[translation];
	$counter = $trans_line[counter];
	if($counter > 2 && 2 <= strlen($translation) && strlen($translation) < 35) {
		echo date('H:m:s').": Обрабатываю перевод '".$translation."', количество совпадений - ".$counter.".\n";
		$orig_query = "SELECT `original`, count(`original`) as `counter` FROM `parser` WHERE `translation` LIKE '".mysql_real_escape_string($translation)."' GROUP BY `original` ORDER BY `counter` DESC LIMIT 1";
		$orig_result = mysql_query($orig_query) or die('Запрос на получение оригиналов не удался: ' . mysql_error());
		while ($orig_line = mysql_fetch_array($orig_result, MYSQL_ASSOC)) {
			$original = $orig_line[original];
			$data = "\n";
			$data .= '# '.$row.' ('.$counter.')'."\n";
			$data .= 'msgid "'.addslashes($original).'"'."\n";
			$data .= 'msgstr "'.addslashes($translation).'"'."\n";
			$file = fopen("base.po", "a");
			fwrite($file, $data);
			fclose ($file);
			// echo $data;
		}
	}
}
mysql_free_result($trans_result);



mysql_close($db_link);
echo date('H:m:s')." -> Готово!"."\n";
?>