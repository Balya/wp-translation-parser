<?php
error_reporting(E_ERROR);
ini_set('memory_limit', '512M');
ini_set('allow_url_fopen','1');

include('simple_html_dom.php');

// echo "<pre>";
$start = "https://ru.wordpress.org/plugins/browse/popular/";


echo date('H:m:s').": Устанавливаю соединение с БД \n";
$db_link = mysql_connect('localhost', 'root', '')
    or die("Не удалось соединиться: " . mysql_error() . "\n");
echo "Соединение c БД успешно установлено \n";
mysql_select_db('wp_translate') or die("Не удалось выбрать базу данных \n");

// SELECT `parser`.`translation`, count(`parser`.`translation`) as cnt FROM `parser` GROUP BY `parser`.`translation` ORDER BY cnt desc limit 0,500


$current_page = $start;
// $i = 0;
do {
	echo date('H:m:s').": Обрабатываю страницу: ".$current_page."\n";
	$current_dom = file_get_html($current_page);


	foreach($current_dom->find('.plugin-card') as $n => $card) {
		$name[$n] = $card->find('h4 a', 0)->innertext;
		$link[$n] = $card->find('h4 a', 0)->href;
		echo "\n".date('H:m:s').": Обрабатываю плагин: ".$name[$n]."\n";
		// echo "Ccылка на плагин: ".$link[$n]."\n";
		$plugin_dom[$n] = file_get_html($link[$n]);
		if ($plugin_dom[$n] && is_object($plugin_dom[$n]) && isset($plugin_dom[$n]->nodes)){
			if ($translate_link[$n] = $plugin_dom[$n]->find('.translations-sidebox p > a', 0)->href) {
				echo date('H:m:s').": Есть страница с переводом. Обрабатываю.\n";
				// echo "Ccылка на страницу с переводом: ".$translate_link[$n]."\n";
				$translate_dom[$n] = file_get_html($translate_link[$n]);
				if ($translate_dom[$n] && is_object($translate_dom[$n]) && isset($translate_dom[$n]->nodes)){
					if ($translate_ru_link[$n] = "https://translate.wordpress.org".$translate_dom[$n]->find('th[title="ru_RU"] a', 0)->href) {
						echo date('H:m:s').": Есть Русская локаль. Обрабатываю.\n";
						// echo "Ccылка на Русскую локаль: ".$translate_ru_link[$n]."\n";
						$translate_project_dom[$n] = file_get_html($translate_ru_link[$n]);
						if ($translate_project_dom[$n] && is_object($translate_project_dom[$n]) && isset($translate_project_dom[$n]->nodes)){
							if ($translate_development_link[$n] = "https://translate.wordpress.org".($translate_project_dom[$n]->find('.locale-project a', 0)->href) ) {
								echo date('H:m:s').": Есть страница разработки. Начинаю дамп переводов.\n";
								// echo "Ccылка на проект разработки: ".$translate_development_link[$n]."\n";

								$current_translate_page = $translate_development_link[$n];
								do {
									echo date('H:m:s').": Обрабатываю страницу: ".$current_translate_page."\n";
									$current_translate_dom = file_get_html($current_translate_page);
									if ($current_translate_dom && is_object($current_translate_dom) && isset($current_translate_dom->nodes)){
										$count[curr] = 0;
										$count[wait] = 0;
										$count[untr] = 0;

										if ($current_translate_dom && is_object($current_translate_dom) && isset($current_translate_dom->nodes)) {
											foreach ($current_translate_dom->find('tr.status-current, tr.status-waiting, tr.untranslated') as $row) {
												$row_id = trim($row->row);
												$original = mysql_escape_string(trim($row->find('td.original', 0)->plaintext));
												$translation = mysql_escape_string(trim($row->find('td.translation', 0)->plaintext));
												$row_class = trim($row->class);
												if (substr_count($row_class,"status-current")) {
													$status = "current";
													$count[curr]++;
												} elseif(substr_count($row_class,"status-waiting")) {
													$status = "waiting";
													$count[wait]++;
												} elseif(substr_count($row_class,"untranslated")) {
													$status = "untranslated";
													$count[untr]++;
												} else {
													$status = "null";
												}
												$row_link = $current_translate_page;

												$query = "INSERT INTO `wp_translate`.`parser` (`row`, `original`, `translation`, `status`, `link`) VALUES ('".$row_id."', '".$original."', '".$translation."', '".$status."', '".$row_link."');";
												mysql_query($query) or die("Запрос не удался: " . mysql_error() . "\n");

												// echo "    ".$row_id." | ".$status." | ".$original." | ".$translation." | ".$row_link."\n\n";
												// echo $query."\n\n";
											}
										} else {
											echo date('H:m:s').": Ошибка при разборе страницы. Пропускаю.\n";
										}

										echo "          Добавлено элментов в базу: ".$count[curr]." с переводом, ".$count[wait]." ожидающие утверждения, ".$count[untr]." без перевода\n";
										$translate_next = $current_translate_dom->find('a.next', 0);
										$current_translate_page = "https://translate.wordpress.org".$current_translate_dom->find('a.next', 0)->href;
									} else {
										echo date('H:m:s').": Ошибка при разборе страницы. Пропускаю.\n";
									}
								} while ($translate_next->href);
							} else {
								echo date('H:m:s').": Нет ссылки на проект разработки. Игнорирую.\n";
							}
						} else {
							echo date('H:m:s').": Ошибка при разборе страницы. Пропускаю.\n";
						}
					} else {
						echo date('H:m:s').": Нет Русской локали. Игнорирую.\n";
					}
				} else {
					echo date('H:m:s').": Ошибка при разборе страницы. Пропускаю.\n";
				}
			} else {
				echo date('H:m:s').": Нет страницы с переводом. Игнорирую.\n";
			}
		} else {
			echo date('H:m:s').": Ошибка при разборе страницы. Пропускаю.\n";
		}
	}

	$next = $current_dom->find('a.next', 0);
	$current_page = "https://ru.wordpress.org".$current_dom->find('a.next', 0)->href;
	// $i++;
	// if ($i > 1) break;
} while ($next->href);










echo date('H:m:s')." -> Готово!"."\n";
mysql_close($db_link);





// echo "</pre>";
?>